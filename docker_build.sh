#!/usr/bin/env bash
set -e

if [ -z $TARGET_VERSION ]; then
    echo "TARGET_VERSION is not available"
    exit -1
fi

cd scriptlets
mvn package
cp target/xivo-scriptlets-$TARGET_VERSION.jar ../docker/lib/
cd ..

docker build --build-arg TARGET_VERSION=$TARGET_VERSION -t xivoxc/spagobi:$TARGET_VERSION docker/
