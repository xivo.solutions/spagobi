package solutions.xivo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DateUtilTest {


    @Test
    @DisplayName("should format interval when null")
    void intervalToTextNullTest() {

        DateUtil s = new DateUtil();

        String fmt = s.intervalToText(null);

        assertEquals("00:00:00", fmt,"unable to format null value");

    }

    @Test
    @DisplayName("should format interval less than 1 day")
    void intervalToTextLessThanOneDayTest() {

        DateUtil s = new DateUtil();

        String fmt = s.intervalToText(55947L);

        assertEquals("15:32:27", fmt,"unable to format interval less than one day");

    }

    @Test
    @DisplayName("should format interval greater than 1 day")
    void intervalToTextGreaterThanOneDayTest() {

        DateUtil s = new DateUtil();

        String fmt = s.intervalToText(1051728L);

        assertEquals("12J 04:08:48", fmt,"unable to format interval greater than one day");

    }



}
