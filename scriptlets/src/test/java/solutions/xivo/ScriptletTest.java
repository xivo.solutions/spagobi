package solutions.xivo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.sql.Array;

import net.sf.jasperreports.engine.JRScriptletException;

class ScriptletTest {

    @Test
    @DisplayName("toHashMap(String[], Long[]) should convert data")
    void toHashMapTest() {
        Scriptlet s = new Scriptlet();
        String[] keys = {"a", "d", "abcd"};
        Long[] values = {1l, 2l, 123l};
        try {
            HashMap<String, Long> m = s.toHashMap(keys, values);

            assertEquals(m.get("a"), new Long(1));
            assertEquals(m.get("d"), new Long(2));
            assertEquals(m.get("abcd"), new Long(123));
            assertEquals(m.size(), 3);
        } catch (JRScriptletException e) {
            fail("Got exception while testing", e);
        }
    }
/*
    @Test
    @DisplayName("toHashMap(String[], Long[]) should fail when arrays size mismatch")
    void toHashMapMismatchTest() {
        Scriptlet s = new Scriptlet();
        String[] keys = {"a", "d", "abcd"};
        Long[] values = {1l, 2l};
        JRScriptletException ex =
                assertThrows(JRScriptletException.class, () -> {
                    s.toHashMap(keys, values);
                });
        assertEquals("Keys & Values array length mismatch", ex.getMessage());
    }
*/
    @Test
    @DisplayName("toHashMap(Array, Array) should convert data")
    void toHashMapArrayTest() {
        Scriptlet s = new Scriptlet();
        String[] keys = {"a", "d", "abcd"};
        Long[] values = {1l, 2l, 123l};
        try {
            Array k = mock(Array.class);
            Array v = mock(Array.class);
            when(k.getArray()).thenReturn(keys);
            when(v.getArray()).thenReturn(values);

            HashMap<String, Long> m = s.toHashMap(k, v);

            assertEquals(m.get("a"), new Long(1));
            assertEquals(m.get("d"), new Long(2));
            assertEquals(m.get("abcd"), new Long(123));
            assertEquals(m.size(), 3);
        } catch (Exception e) {
            fail("Got exception while testing", e);
        }
    }

    @Test
    @DisplayName("sum() should compute sum of Array of Long")
    void sumTest() {
        Long[] values = {1l, 2l, 5l, 10l};
        Scriptlet s = new Scriptlet();
        Array a = mock(Array.class);
        try {
            when(a.getArray()).thenReturn(values);
            Long result = s.sum(a);
            assertEquals(result, new Long(18l));
        } catch (Exception e) {
            fail("Exception when testing sum", e);
        }

    }

    @Test
    @DisplayName("mapGetLong should")
    void mapGetLongTest() {
        Scriptlet s = new Scriptlet();
        HashMap<String, Long> m = new HashMap<String, Long>();
        m.put("a", 1l);

        assertEquals(s.mapGetLong(m, "a"), new Long(1), "extract existing value for a given key");
        assertEquals(s.mapGetLong(m, "b"), new Long(0), "return 0 for non existing key");
    }

    @Test
    @DisplayName("toThresholdMap(Array, Array) should convert data and fill empty values")
    void toThresholdMapTest() {
        Scriptlet s = new Scriptlet();
        String[] keys = {"t1", "t3", "t5"};
        Long[] values = {1l, 2l, 123l};
        try {
            Array k = mock(Array.class);
            Array v = mock(Array.class);
            when(k.getArray()).thenReturn(keys);
            when(v.getArray()).thenReturn(values);

            HashMap<String, Long> m = s.toThresholdMap("t", 6, k, v);

            assertEquals(m.get("t1"), new Long(1));
            assertEquals(m.get("t2"), new Long(0));
            assertEquals(m.get("t3"), new Long(2));
            assertEquals(m.get("t4"), new Long(0));
            assertEquals(m.get("t5"), new Long(123));
            assertEquals(m.get("t6"), new Long(0));
            assertEquals(m.size(), 6);
        } catch (Exception e) {
            fail("Got exception while testing", e);
        }
    }

}
