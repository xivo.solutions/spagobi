package solutions.xivo;

import java.util.HashMap;
import java.sql.Array;
import java.sql.SQLException;
import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

public class Scriptlet extends JRDefaultScriptlet {


    public HashMap<String, Long> toHashMap(String[] keys, Long[] values) throws JRScriptletException {
        HashMap<String, Long> map = new HashMap<String, Long>();
        if(keys.length != values.length) {
            throw new JRScriptletException("Keys & Values array length mismatch");
        }
        
        for (int i=0; i<keys.length; i++) {
            map.put(keys[i], values[i]);
        }
        return map;
    }

    public HashMap<String, Long> toHashMap(Array keys, Array values) throws JRScriptletException {
        try {
            return toHashMap((String[]) keys.getArray(), (Long[]) values.getArray());
        } catch (SQLException e) {
            throw new JRScriptletException(e);
        }
    }

    public Long sum(Array values) throws JRScriptletException {
        if(values == null) return 0l;
        
        try {
            Long[] data = (Long[]) values.getArray();
            long sum = 0;
            for(Long e : data) {
                sum += e;
            }
            return sum;
        } catch (SQLException e) {
            throw new JRScriptletException(e);
        }

    }

    public Long mapGetLong(HashMap<String, Long> map, String key) {
        if(map == null) return 0l;
        if(map.containsKey(key))
            return map.get(key);
        else
            return 0l;
    }

    public HashMap<String, Long> toThresholdMap(String prefix, Integer thresholdCount, Array keys, Array values) throws JRScriptletException {
        HashMap<String, Long> map = toHashMap(keys, values);
        for(int i=1; i<=thresholdCount; i++) {
            String k = String.format("%s%d", prefix, i);
            if(!map.containsKey(k))
                map.put(k, 0l);
        }
        return map;
    }

}
