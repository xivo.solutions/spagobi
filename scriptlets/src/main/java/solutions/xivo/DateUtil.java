package solutions.xivo;

import net.sf.jasperreports.engine.JRDefaultScriptlet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil extends JRDefaultScriptlet {
    private static long Hour = 3600;
    private static long millisecond = 1000;


    public String intervalToText(Long interval) {

        if (interval == null)
            return "00:00:00";

        if (interval >= 24 * Hour) {
            Long days = interval / (24 * Hour);
            return String.format("%dJ ", days) + intervalToText(interval - days * 24 * Hour);
        }

        Date d = new Date(interval * millisecond);
        SimpleDateFormat fmt = new SimpleDateFormat("HH:mm:ss");

        fmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        return fmt.format(d);
    }
}
