#Jasper Report Scriplets
 
 
 These scriplets are needed to run custom reports
 
 ## How to build
 
 mvn clean package -Denv.TARGET_VERSION="9999"
 
 ## Install in Jasper Studio
 
 Menu project -> Properties -> Java Build Path
 
 Add xivo-scriplet-X.X.jar as an external library

## Manual Installation in SpagoBi (when not built in)

- Copy target/xivo-scriplets-X.X.jar to /etc/docker/compose/spagobi/
- Change the spagobi/volumes section in the `/etc/docker/compose/docker-xivocc.yml` file to add a custom volume mapping: 

``` 
volumes:
  - /var/log/xivocc/spagobi:/usr/local/tomcat/logs
  - /etc/docker/compose/spagobi/xivo-scriptlets-X.X.jar:/usr/local/tomcat/webapps/SpagoBIJasperReportEngine/WEB-INF/lib/xivo-scriptlet-X.X.jar

```
- Recreate spagobi container `xivocc-dcomp up -d`


## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the [COPYING](COPYING) and [COPYING.LESSER](COPYING.LESSER) files for details.
