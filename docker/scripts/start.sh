#!/bin/bash

updatedb()  {
    echo "[INFO] - updating database"
    cd /opt/liquibase/scripts/
    liquibase --driver="org.postgresql.Driver" \
        --changeLogFile=db.changelog.xml \
        --url="jdbc:postgresql://db/spagobi" \
        --username=spagobi --password=spagobi  \
        --classpath=$CATALINA_HOME/lib/postgresql-$POSTGRES_DRIVER_VERSION.jar \
        --logLevel=info \
        migrate
    return $?
}

i=1
max=5
sec=10

updatedb
while [ $? -ne 0 ] && [ $i -lt $max ]
do
    echo "[ERROR] -  spagobi ["$i"] unable to update database waiting $sec seconds"
    i=$(( $i + 1 ))
    sleep $sec
    updatedb
done

if [ $i -eq $max ]
then
    echo "[ERROR] - spagobi unable to update database tried $i time"
    exit -1
fi

echo "[INFO] - Starting spago bi"
$CATALINA_HOME/bin/catalina.sh run
exit 0
