#!/bin/bash

echo "***updating dababase"
liquibase --driver=com.mysql.jdbc.Driver \
     --classpath=/path/to/classes \
     --changeLogFile=com/example/db.changelog.xml \
     --url="jdbc:mysql://db/spagobi" \
     --username=spagobi \
     --password=spagobi \
     migrate
exit 0
