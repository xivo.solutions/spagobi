FROM tomcat:7.0.85-jre8-slim

ENV SPAGOBI_VERSION 5.0.0_15092014

LABEL spagobi_version=${SPAGOBI_VERSION}

RUN rm /etc/apt/sources.list ; \
    echo "deb http://archive.debian.org/debian stretch main" > /etc/apt/sources.list ; \
    apt update && apt install -y gnupg ; \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 648ACFD622F3D138 0E98404D386FA1D9 ; \
    echo "deb http://archive.debian.org/debian-security stretch/updates main" >> /etc/apt/sources.list ; \
    echo "deb http://archive.debian.org/debian stretch-proposed-updates main" >> /etc/apt/sources.list

RUN apt-get update \
    && apt-get install -y --no-install-recommends --auto-remove \
    patch \
    unzip \
    wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN sed -i 's/^assistive_technologies=/#&/' /etc/java-8-openjdk/accessibility.properties

RUN wget --no-verbose http://dllegacy.ow2.org/spagobi/SpagoBI-bin-$SPAGOBI_VERSION.zip -O /tmp/SpagoBI-bin-$SPAGOBI_VERSION.zip && \
    unzip -o -d $CATALINA_HOME/webapps /tmp/SpagoBI-bin-$SPAGOBI_VERSION.zip && \
    unzip -q $CATALINA_HOME/webapps/SpagoBI.war -d $CATALINA_HOME/webapps/SpagoBI && \
    rm -f $CATALINA_HOME/webapps/SpagoBI.war && \
    rm -f /tmp/SpagoBI-bin-$SPAGOBI_VERSION.zip

RUN wget --no-verbose http://dllegacy.ow2.org/spagobi/SpagoBIBirtReportEngine-bin-$SPAGOBI_VERSION.zip -O /tmp/SpagoBIBirtReportEngine-bin-$SPAGOBI_VERSION.zip && \
    unzip -o -d $CATALINA_HOME/webapps /tmp/SpagoBIBirtReportEngine-bin-$SPAGOBI_VERSION.zip && \
    unzip -q $CATALINA_HOME/webapps/SpagoBIBirtReportEngine.war -d $CATALINA_HOME/webapps/SpagoBIBirtReportEngine && \
    rm -f $CATALINA_HOME/webapps/SpagoBIBirtReportEngine.war && \
    rm -f /tmp/SpagoBIBirtReportEngine-bin-$SPAGOBI_VERSION.zip

RUN wget --no-verbose http://dllegacy.ow2.org/spagobi/SpagoBIChartEngine-bin-$SPAGOBI_VERSION.zip -O /tmp/SpagoBIChartEngine-bin-$SPAGOBI_VERSION.zip && \
    unzip -o -d $CATALINA_HOME/webapps /tmp/SpagoBIChartEngine-bin-$SPAGOBI_VERSION.zip && \
    unzip -q $CATALINA_HOME/webapps/SpagoBIChartEngine.war -d $CATALINA_HOME/webapps/SpagoBIChartEngine && \
    rm -f $CATALINA_HOME/webapps/SpagoBIChartEngine.war && \
    rm -f /tmp/SpagoBIChartEngine-bin-$SPAGOBI_VERSION.zip

RUN wget --no-verbose http://dllegacy.ow2.org/spagobi/SpagoBIJasperReportEngine-bin-$SPAGOBI_VERSION.zip -O /tmp/SpagoBIJasperReportEngine-bin-$SPAGOBI_VERSION.zip && \
    unzip -o -d $CATALINA_HOME/webapps /tmp/SpagoBIJasperReportEngine-bin-$SPAGOBI_VERSION.zip && \
    unzip -q $CATALINA_HOME/webapps/SpagoBIJasperReportEngine.war -d $CATALINA_HOME/webapps/SpagoBIJasperReportEngine && \
    rm -f $CATALINA_HOME/webapps/SpagoBIJasperReportEngine.war && \
    rm -f /tmp/SpagoBIJasperReportEngine-bin-$SPAGOBI_VERSION.zip

RUN wget --no-verbose http://dllegacy.ow2.org/spagobi/SpagoBIQbeEngine-bin-$SPAGOBI_VERSION.zip -O /tmp/SpagoBIQbeEngine-bin-$SPAGOBI_VERSION.zip && \
    unzip -o -d $CATALINA_HOME/webapps /tmp/SpagoBIQbeEngine-bin-$SPAGOBI_VERSION.zip && \
    unzip -q $CATALINA_HOME/webapps/SpagoBIQbeEngine.war -d $CATALINA_HOME/webapps/SpagoBIQbeEngine && \
    rm -f $CATALINA_HOME/webapps/SpagoBIQbeEngine.war && \
    rm -f /tmp/SpagoBIQbeEngine-bin-$SPAGOBI_VERSION.zip

RUN wget --no-verbose http://dllegacy.ow2.org/spagobi/SpagoBITalendEngine-bin-$SPAGOBI_VERSION.zip -O /tmp/SpagoBITalendEngine-bin-$SPAGOBI_VERSION.zip && \
    unzip -o -d $CATALINA_HOME/webapps /tmp/SpagoBITalendEngine-bin-$SPAGOBI_VERSION.zip && \
    unzip -q $CATALINA_HOME/webapps/SpagoBITalendEngine.war -d $CATALINA_HOME/webapps/SpagoBITalendEngine && \
    rm -f $CATALINA_HOME/webapps/SpagoBITalendEngine.war && \
    rm -f /tmp/SpagoBITalendEngine-bin-$SPAGOBI_VERSION.zip

ENV POSTGRES_DRIVER_VERSION 42.2.4
COPY ./lib/postgresql-$POSTGRES_DRIVER_VERSION.jar $CATALINA_HOME/lib/

ADD ./tomcat/conf/server.xml $CATALINA_HOME/conf/server.xml

ADD ./lib/jasperreports-6.1.1.jar /tmp/jasperreports-6.1.1.jar
ADD ./lib/jasperreports-fonts-6.1.1.jar /tmp/jasperreports-fonts-6.1.1.jar
ADD ./lib/jasperreports-functions-6.1.1.jar /tmp/jasperreports-functions-6.1.1.jar
ADD ./lib/jasperreports-javaflow-6.1.1.jar /tmp/jasperreports-javaflow-6.1.1.jar
ADD ./lib/jasperreports-chart-themes-6.1.1.jar /tmp/jasperreports-chart-themes-6.1.1.jar
ADD ./lib/joda-time-2.4.jar /tmp/joda-time-2.4.jar
ADD ./lib/itextpdf-5.4.1.jar /tmp/itextpdf-5.4.1.jar
ADD ./lib/itext-pdfa-5.4.0.jar /tmp/itext-pdfa-5.4.0.jar

ARG TARGET_VERSION
ADD ./lib/xivo-scriptlets-$TARGET_VERSION.jar /tmp/xivo-scriptlets-${TARGET_VERSION}.jar 

ADD ./patches/login_page.patch /tmp/login_page.patch

ADD ./scripts/install /root/install

ENV LOGDIR="/opt/docker/var/log"
ENV CATALINA_OPTS="$CATALINA_OPTS -Duser.timezone=UTC"

RUN chmod 755 /root/install
RUN /root/install

EXPOSE 8080

# liquibase
ENV LIQUIBASE_VERSION 3.4.1

RUN wget --no-verbose https://github.com/liquibase/liquibase/releases/download/liquibase-parent-$LIQUIBASE_VERSION/liquibase-$LIQUIBASE_VERSION-bin.tar.gz -O /tmp/liquibase-$LIQUIBASE_VERSION-bin.tar.gz \
    && mkdir /opt/liquibase \
    && tar -xzf /tmp/liquibase-$LIQUIBASE_VERSION-bin.tar.gz -C /opt/liquibase \
    && chmod +x /opt/liquibase/liquibase \
    && ln -s /opt/liquibase/liquibase /usr/local/bin/ \
    && ln -s $CATALINA_HOME/lib/postgresql-$POSTGRES_DRIVER_VERSION.jar /usr/local/bin/

ADD ./sql /opt/liquibase/scripts

ADD ./scripts/start.sh /root/start.sh

LABEL version=${TARGET_VERSION}

CMD /root/start.sh
